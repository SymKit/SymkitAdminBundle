<?php

namespace Symkit\AdminBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SymkitAdminBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'SonataUserBundle';
    }
}
